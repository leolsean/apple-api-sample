package org.leolsean.exception;

public class PaymentNotReceivedException extends Exception {
    public PaymentNotReceivedException(String message) {
        super(message);
    }
}
