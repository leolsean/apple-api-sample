package org.leolsean.exception;

public class ExpiredException extends Exception {
    public ExpiredException(String message) {
        super(message);
    }
}
