package org.leolsean.exception;

public class CompletedException extends Exception {
    public CompletedException(String message) {
        super(message);
    }
}
