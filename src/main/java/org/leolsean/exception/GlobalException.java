package org.leolsean.exception;

public class GlobalException extends RuntimeException {
    private String retCode;
    private String retMsg;


    public GlobalException(String retCode) {
        super(retCode);
        this.retCode = retCode;
    }

    public GlobalException(Exception e) {
        super(e);
    }

    public GlobalException(Exception e, String retMsg) {
        super(e);
        this.retMsg = retMsg;
    }

    public GlobalException(String retCode, String retMsg) {
        super(retCode);
        this.retCode = retCode;
        this.retMsg = retMsg;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    public String getRetCode() {
        return retCode;
    }
}
