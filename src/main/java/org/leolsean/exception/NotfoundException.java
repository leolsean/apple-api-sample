package org.leolsean.exception;

public class NotfoundException extends Exception {
    public NotfoundException(String message) {
        super(message);
    }
}
