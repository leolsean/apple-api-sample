package org.leolsean.exception;

public class UnknownException extends Exception {
    public UnknownException(String message) {
        super(message);
    }
}
