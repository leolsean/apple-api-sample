package org.leolsean.exception;

public class ConfigurationException extends Exception {
    public ConfigurationException(String message) {
        super(message);
    }
}
