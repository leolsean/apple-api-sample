package org.example;

import java.util.List;
import org.junit.Test;
import org.leolsean.apple.ApplePublisher;
import org.leolsean.exception.ConfigurationException;
import org.leolsean.exception.InternalException;
import org.leolsean.exception.NotfoundException;

/**
 * Unit test for simple Main.
 */
public class AppTest {

  /**
   * Rigorous Test :-)
   */
  @Test
  public void test()
      throws NotfoundException, ConfigurationException, InternalException {
    // 如下三个参数请替换
    String packageName = "com.test.apple";
    String keyId = "AAAAAAA";
    String issuer = "9bbd36ec-CCCC-4e83-92f2-BBBBB";

    String originalTransactionId = "570000912800778";
    // 获取交易历史
    ApplePublisher.TransactionHistory history = ApplePublisher
        .getTransactionHistory(packageName, keyId, issuer, originalTransactionId);
    List<String> payload = history.decodePayload(history.getSignedTransactions(), 0, String.class);
    System.out.println("获取交易历史: " + payload);

    System.out.println("\n\n");
    // 获取所有订阅状态
    ApplePublisher.SubscriptionStatuses statuses = ApplePublisher
        .getAllSubscriptionStatuses(packageName, keyId, issuer, originalTransactionId);
    List<ApplePublisher.SubscriptionStatuses.SubscriptionData> dataList = statuses.getData();
    System.out.println("获取所有订阅状态!");
    for (ApplePublisher.SubscriptionStatuses.SubscriptionData data : dataList) {
      System.out.println("id: " + data.getSubscriptionGroupIdentifier());
      List<ApplePublisher.SubscriptionStatuses.SubscriptionData.LastTransactions> transactions = data
          .getLastTransactions();
      for (ApplePublisher.SubscriptionStatuses.SubscriptionData.LastTransactions lastTransactions : transactions) {
        System.out.println("OriginalTransactionId: " + lastTransactions.getOriginalTransactionId());
        System.out.println(" status: " + lastTransactions.getStatus());
        System.out.println(" TransactionInfo: " + lastTransactions
            .decodePayload(new String[]{lastTransactions.getSignedTransactionInfo()}, -1,
                String.class));
        System.out.println(" RenewalInfo: " + lastTransactions
            .decodePayload(new String[]{lastTransactions.getSignedRenewalInfo()}, -1,
                String.class));
      }
    }

    System.out.println("\n\n");

    // 通过用户侧的订单号查询订单 - 如下三个参数请替换
    packageName = "com.test.apple";
    keyId = "XXXX";
    issuer = "ea6";

    String orderId = "MNTB19859K";
    ApplePublisher.Order order = ApplePublisher.lookUpOrderID(packageName, keyId, issuer, orderId);
    System.out.println(order.decodePayload(order.getSignedTransactions(), -1, String.class));
  }
}
